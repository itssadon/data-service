package execute

import (
	"context"
	strip "github.com/grokify/html-strip-tags-go"
	"gitlab.com/itssadon/data-service/database"
	"log"
	"time"
)

func runInsert(insertContent Insert) (int, error) {
	insertResource := strip.StripTags(insertContent.Resource)
	insertColumns := strip.StripTags(insertContent.Columns)
	insertValues := strip.StripTags(insertContent.Values)

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	sqlQuery := `INSERT INTO ` + insertResource + `(` + insertColumns + `) VALUES (?)`
	result, err := database.DBConn.ExecContext(ctx, sqlQuery, insertValues)
	if err != nil {
		log.Println(err.Error())
		return 0, err
	}

	insertID, err := result.LastInsertId()
	if err != nil {
		log.Println(err.Error())
		return 0, err
	}

	return int(insertID), nil
}
