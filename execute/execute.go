package execute

type Insert struct {
	Resource string `json:"resource"`
	Columns string `json:"columns"`
	Values string `json:"values"`
}
