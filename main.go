package main

import (
	"encoding/json"
	"gitlab.com/itssadon/data-service/query"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	"gitlab.com/itssadon/data-service/database"
	"gitlab.com/itssadon/data-service/resource"
)

type ResponseObj struct {
	Status  bool   `json:"status"`
	Message string `json:"message"`
}

func baseHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		response := ResponseObj{true, "Welcome to Data Service"}

		js, err := json.Marshal(response)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_, err = w.Write(js)
		if err != nil {
			log.Fatal(err)
		}
	}
}

const serviceBasePath = "/"

func main() {
	// load .env file
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	database.SetupDatabase()
	http.HandleFunc(serviceBasePath, baseHandler)
	resource.SetupRoutes(serviceBasePath)
	query.SetupRoutes(serviceBasePath)
	log.Fatal(http.ListenAndServe(":5000", nil))
}
