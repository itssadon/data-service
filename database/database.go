package database

import (
	"database/sql"
	"log"
	"os"
	"time"
)

var DBConn *sql.DB

func SetupDatabase() {
	var dbConnection = os.Getenv("DB_CONNECTION")
	var dbUser = os.Getenv("DB_USERNAME")
	var dbPassword = os.Getenv("DB_PASSWORD")
	var dbHost = os.Getenv("DB_HOST")
	var dbPort = os.Getenv("DB_PORT")
	var dbName = os.Getenv("DB_DATABASE")

	var connString = dbUser + ":" + dbPassword + "@tcp(" + dbHost + ":" + dbPort + ")/" + dbName

	var err error
	DBConn, err = sql.Open(dbConnection, connString)
	if err != nil {
		log.Fatal(err)
	}
	DBConn.SetMaxOpenConns(4)
	DBConn.SetConnMaxIdleTime(4)
	DBConn.SetConnMaxLifetime(60 * time.Second)
}
