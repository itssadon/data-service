package resource

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
)

const tablesPath = "tables"
const tableColumnsPath = "table"

type ResponseObj struct {
	Status  bool   `json:"status"`
	Message string `json:"message"`
}

func handleTableList(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		tableList, err := getTableList()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		if len(tableList) == 0 {
			response := ResponseObj{false, "No tables found"}

			js, err := json.Marshal(response)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			w.WriteHeader(http.StatusNotFound)
			_, err = w.Write(js)
			if err != nil {
				log.Fatal(err)
			}
		}

		newList := make([]string, 0)
		for _, s := range tableList {
			newList = append(newList, s.Name)
		}

		tablesJson, err := json.Marshal(newList)
		if err != nil {
			log.Fatal(err)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_, err = w.Write(tablesJson)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func handleTableColumnsList(w http.ResponseWriter, r *http.Request) {
	urlPathSegments := strings.Split(r.URL.Path, fmt.Sprintf("%s/", tableColumnsPath))
	if len(urlPathSegments[1:]) > 1 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	tableName := urlPathSegments[1]
	switch r.Method {
	case http.MethodGet:
		tableColumnsList, err := getTableColumns(tableName)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		if len(tableColumnsList) == 0 {
			response := ResponseObj{false, "No table columns found"}

			js, err := json.Marshal(response)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			w.WriteHeader(http.StatusNotFound)
			_, err = w.Write(js)
			if err != nil {
				log.Fatal(err)
			}
		}

		tableColumnsJson, err := json.Marshal(tableColumnsList)
		if err != nil {
			log.Fatal(err)
			return
		}

		w.WriteHeader(http.StatusOK)
		_, err = w.Write(tableColumnsJson)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func SetupRoutes(serviceBasePath string) {
	tableListHandler := http.HandlerFunc(handleTableList)
	tablesColumnsListHandler := http.HandlerFunc(handleTableColumnsList)

	http.Handle(fmt.Sprintf("%s%s", serviceBasePath, tablesPath), tableListHandler)
	http.Handle(fmt.Sprintf("%s%s/", serviceBasePath, tableColumnsPath), tablesColumnsListHandler)
}
