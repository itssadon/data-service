package resource

import (
	"context"
	"database/sql"
	"log"
	"time"

	"gitlab.com/itssadon/data-service/database"
)

func getTableList() ([]Table, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	results, err := database.DBConn.QueryContext(ctx, `SHOW TABLES`)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	defer func(results *sql.Rows) {
		err := results.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(results)

	tables := make([]Table, 0)
	for results.Next() {
		var table Table
		err := results.Scan(&table.Name)
		if err != nil {
			return nil, err
		}

		tables = append(tables, table)
	}
	return tables, nil
}

func getTableColumns(tableName string) ([]Column, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	results, err := database.DBConn.QueryContext(ctx, `SHOW COLUMNS FROM `+tableName)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	defer func(results *sql.Rows) {
		err := results.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(results)

	tableColumns := make([]Column, 0)
	for results.Next() {
		var tableColumn Column
		err := results.Scan(&tableColumn.Field,
			&tableColumn.Type,
			&tableColumn.Null,
			&tableColumn.Key,
			&tableColumn.Default,
			&tableColumn.Extra)
		if err != nil {
			return nil, err
		}

		tableColumns = append(tableColumns, tableColumn)
	}
	return tableColumns, nil
}
