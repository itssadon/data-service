module gitlab.com/itssadon/data-service

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/grokify/html-strip-tags-go v0.0.1 // indirect
	github.com/joho/godotenv v1.3.0
)
