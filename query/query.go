package query

type Query struct {
	Resource string `json:"resource"`
	Columns string `json:"columns"`
	Conditions string `json:"conditions"`
}

type RawQuery struct {
	QueryDefinition string `json:"query_definition"`
}
