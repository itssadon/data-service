package query

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

const queryPath = "query"
const rawQueryPath = "query/raw"

type ResponseObj struct {
	Status  bool   `json:"status"`
	Message string `json:"message"`
}

func handleQuery(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		var queryContent Query
		err := json.NewDecoder(r.Body).Decode(&queryContent)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		queryRows, err := runQuery(queryContent)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		if len(queryRows) == 0 {
			response := ResponseObj{false, "No data in table"}

			js, err := json.Marshal(response)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			w.WriteHeader(http.StatusNotFound)
			_, err = w.Write(js)
			if err != nil {
				log.Fatal(err)
			}
		}

		tablesJson, err := json.Marshal(queryRows)
		if err != nil {
			log.Fatal(err)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_, err = w.Write(tablesJson)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func handleRawQuery(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		var queryContent RawQuery
		err := json.NewDecoder(r.Body).Decode(&queryContent)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		queryRows, err := runRawQuery(queryContent)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		if len(queryRows) == 0 {
			response := ResponseObj{false, "No data in table"}

			js, err := json.Marshal(response)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			w.WriteHeader(http.StatusNotFound)
			_, err = w.Write(js)
			if err != nil {
				log.Fatal(err)
			}
		}

		tablesJson, err := json.Marshal(queryRows)
		if err != nil {
			log.Fatal(err)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_, err = w.Write(tablesJson)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func SetupRoutes(serviceBasePath string) {
	queryHandler := http.HandlerFunc(handleQuery)
	rawQueryHandler := http.HandlerFunc(handleRawQuery)

	http.Handle(fmt.Sprintf("%s%s", serviceBasePath, queryPath), queryHandler)
	http.Handle(fmt.Sprintf("%s%s", serviceBasePath, rawQueryPath), rawQueryHandler)
}