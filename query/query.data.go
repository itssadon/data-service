package query

import (
	"context"
	"database/sql"
	strip "github.com/grokify/html-strip-tags-go"
	"gitlab.com/itssadon/data-service/database"
	"log"
	"time"
)

func runQuery(queryContent Query) ([]map[string]interface{}, error) {
	queryResource := strip.StripTags(queryContent.Resource)
	queryColumns := strip.StripTags(queryContent.Columns)
	queryConditions := strip.StripTags(queryContent.Conditions)

	columns := "*"
	if queryColumns != "" {
		columns = queryColumns
	}

	conditions := ""
	if queryConditions != "" {
		conditions = " WHERE " + queryConditions
	}

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := "SELECT " + columns + " FROM " + queryResource + conditions
	results, err := database.DBConn.QueryContext(ctx, query)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	defer func(results *sql.Rows) {
		err := results.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(results)

	cols, err := results.Columns()
	if err != nil {
		return nil, err
	}

	var allMaps []map[string]interface{}
	for results.Next() {
		// Create a slice of interface{}'s to represent each column,
		// and a second slice to contain pointers to each item in the columns slice.
		columns := make([]interface{}, len(cols))
		columnPointers := make([]interface{}, len(cols))
		for i := range columns {
			columnPointers[i] = &columns[i]
		}

		// Scan the result into the column pointers...
		if err := results.Scan(columnPointers...); err != nil {
			return nil, err
		}

		// Create our map, and retrieve the value for each column from the pointers slice,
		// storing it in the map with the name of the column as the key.
		m := make(map[string]interface{})
		for i, colName := range cols {
			val := columnPointers[i].(*interface{})
			n := *val
			colVal := ""
			if n != nil {
				colVal = b2s(n.([]uint8))
			}

			m[colName] = colVal
		}

		allMaps = append(allMaps, m)
	}

	return allMaps, nil
}

func runRawQuery(queryContent RawQuery) ([]map[string]interface{}, error) {
	queryDefinition := strip.StripTags(queryContent.QueryDefinition)

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	results, err := database.DBConn.QueryContext(ctx, queryDefinition)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	defer func(results *sql.Rows) {
		err := results.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(results)

	cols, err := results.Columns()
	if err != nil {
		return nil, err
	}

	var allMaps []map[string]interface{}
	for results.Next() {
		// Create a slice of interface{}'s to represent each column,
		// and a second slice to contain pointers to each item in the columns slice.
		columns := make([]interface{}, len(cols))
		columnPointers := make([]interface{}, len(cols))
		for i := range columns {
			columnPointers[i] = &columns[i]
		}

		// Scan the result into the column pointers...
		if err := results.Scan(columnPointers...); err != nil {
			return nil, err
		}

		// Create our map, and retrieve the value for each column from the pointers slice,
		// storing it in the map with the name of the column as the key.
		m := make(map[string]interface{})
		for i, colName := range cols {
			val := columnPointers[i].(*interface{})
			n := *val
			colVal := ""
			if n != nil {
				colVal = b2s(n.([]uint8))
			}

			m[colName] = colVal
		}

		allMaps = append(allMaps, m)
	}

	return allMaps, nil
}

func b2s(bs []uint8) string {
	b := make([]byte, len(bs))
	for i, v := range bs {
		b[i] = byte(v)
	}
	return string(b)
}
