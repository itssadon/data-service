# Data Service 

GO Implementation of the Internal DB Service at Glade

## How to use

- Rename `.env.example` to `.env`
- Change connection details in `.env` file as desired
- Navigate to the project root in terminal
- Run `go run ./main.go` in terminal
